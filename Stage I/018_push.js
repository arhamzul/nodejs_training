console.log('Pushing data into array');

var persons = [];

var person1 = {
    name: "Ali",
    age: 25,
    role: "admin",
};

var person2 = {
    name: "Siti",
    age: 22,
    role: "user",
};

persons.push(person1);
persons.push(person2);

console.log(persons);