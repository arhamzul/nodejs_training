1. Let user add person through terminal
    >> node abc.js --name="Ali" --age="25" --role="admin"

2. Add new person into person.js  (in 019 directory);
    a. check file exist or not. if not, create an empty one
    b. read its content.
    c. parse it.
    d. push new object into array
    e. stringify it and store into file

3. Console out latest list of person