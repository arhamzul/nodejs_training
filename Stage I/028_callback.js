// Callback concept...
// Function yang terima value untuk dia process, let say get user record based on department
// SELECT * FROM staff WHERE dept=btm
// The function may request this data from database.
// Database then return the query result
// The function then massage the data (query results) and decides to return 3 things...
// 1. The records, 2. Total number of records, 3. Error (if try catch fail)
// So the function return these 3 things to the caller through 'callback'
// The caller than, can pass these three things to its internal function for further use.

var getStaffs = function (dept, callback) {
    // connect with database and get the staff records. Assuming this is the returns.
    var staff = [{
            name: 'Ali',
            role: 'user',
            status: 'active'
        },
        {
            name: 'Siti',
            role: 'admin',
            status: 'active'
        },
        {
            name: 'Minah',
            role: 'admin',
            status: 'deactivated'
        },
        {
            name: 'Husin',
            role: 'user',
            status: 'active'
        },
    ];

    var totalRecord = staff.length;

    var error = false;
    if (totalRecord === 0) {
        error = true;
    }

    callback(staff, totalRecord, error);
};

getStaffs('btm', (records, total, error) => {
    if (error) {
        console.log('Error: Record not found');
    } else {
        console.log('Found ' + total + ' records');
        console.log('----- RECORD ------');
        console.log(records);
    }
});

// why not use return?
// return limit to 1
// return will not pass the value to the function caller but to the variable assigned with the caller 

function kiraLuas(panjang, lebar) {
    var result = panjang * lebar;
    return result;
}
var luas = kiraLuas(2, 3);
console.log(luas);

// Normal return and callback is different concept.
// A simple function will return value to the caller.

// A callback is necessary in js, because its assync nature.
// In php, we will create a function to get the users record
// Then, based on what the model (db) return, we will process it.
// This is possible because PHP is sync langauge. A line will be process
// completely before the next line will be processed.

// if we do the same technique in JS, the second line (processing part) will
// be executed right after the first line (fetching data from DB) without waiting
// the first line to get the data, resulting error (undefined).

// By using callback, we could replicate what PHP could in JS.
// Refer line 45, it is just a function call. It must be completed in one go... which mean..
// getting the records, massage the record THEN using the record. (in sequence.)