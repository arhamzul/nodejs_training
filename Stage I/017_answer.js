console.log('Answer Exercise 017 starting');

const fs = require('fs');

// our object person
var person = {
    name: "Ali",
    age: 25,
    role: "admin",
    status: "active"
};

// convert javascript object into a string
var personString = JSON.stringify(person);

// store this string into file
fs.writeFileSync('017/person.txt', personString);
console.log('Person record has been saved into 017/person.txt');

// read this file
var myString = fs.readFileSync('017/person.txt');
// convert to text
var myText = myString.toString();
console.log(myText);
// convert to json
var myPerson = JSON.parse(myString);
console.log(myPerson.name, myPerson.role);

// read person.txt and transfer its content into person.json
fs.writeFileSync('017/person.json', myString);