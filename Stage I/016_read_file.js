console.log('Reading File...');

const fs = require('fs');

var myString = fs.readFileSync('015/mytext.txt');
console.log(myString);

// it will return buffer. So we have to convert to string

var myText = myString.toString();
console.log(myText);