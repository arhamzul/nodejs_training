// we will learn how to let our app receive input from user (terminal)

console.log('011 User Input - Starting');

// to view what process argument involved while we running this app, try:
console.log(process.argv);

// and run this file 
// node 011_user_input.js SomeText 

// and check the array in terminal

// -----

// let say, we will accept list as command
// node 011_user_input.js list
var command = process.argv[2];
if (command === 'list') {
    console.log('Listing all files');
} else {
    console.log('Command not found');
}

// Exercise
// How to accept 2 arguments
// list, all        => Listing all files
// list, last       => Listiing last file only
// list, first      => Listing first file
// memory, free     => Free memory:
// memory, double   => Wanna double up memory?
// memory, dump     => Dumping memory


// ---- answer ----

// accepeting multiple arg.
var cmd1 = process.argv[2];
var cmd2 = process.argv[3];


if (cmd1 === 'list' && cmd2 === 'all') {
    console.log('Listing all files');
} else if (cmd1 === 'list' && cmd2 === 'last') {
    console.log('Listing last command only');
} else if (cmd1 === 'list' && cmd2 === 'first') {
    console.log('Listing first command');
} else if (cmd1 === 'memory' && cmd2 === 'free') {
    console.log('Free memory :');
} else if (cmd1 === 'memory' && cmd2 === 'double') {
    console.log('wanna double up memory?');
} else if (cmd1 === 'memory' && cmd2 === 'dump') {
    console.log('Dumping all memories');
} else {
    console.log('Command not found');
}