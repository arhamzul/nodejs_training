console.log('note.js is running');

const fs = require('fs');

// function add file (ES6)
var addNote = (note, flag) => {
    fs.appendFileSync('013/note.txt', `${note} (${flag})` + "\n");
};

// function list all notes (old school function method)
var getAll = function () {
    console.log('Get all notes');
};

// export the function
module.exports = {
    addNote,
    getAll
};