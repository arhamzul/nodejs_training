console.log('Using rimraf');

const fs = require('fs');
const rimraf = require('rimraf');

// lets create a temp directory first
var dir = '009';
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
fs.appendFileSync('009/hello.txt', 'Hello Hello MOF');
fs.appendFileSync('009/a.txt', 'Hello Hello MOF');
fs.appendFileSync('009/b.txt', 'Hello Hello MOF');

// lets delete this 009 directory, even though it has files in here
rimraf('009', function () {
    console.log('Folder 009 and its content dah dipadam');
});