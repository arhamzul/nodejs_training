console.log('Lodash starting...');

const _ = require('lodash');

// refer https://lodash.com/ untuk tengok apa yang lodash boleh buat
// isString is a function in lodash, which it will return true if the argument is a text (string)

var myString = 'Treasury';
var myNumber = 125;

if (_.isString(myString)) {
    console.log('this is a string');
}

if (_.isString(myNumber) == false) {
    console.log('this is a number');
}