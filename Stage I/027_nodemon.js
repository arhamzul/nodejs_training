// install nodemon to monitor source code
// >> npm install -g nodemon 
// >> nodemon 27_nodemon.js
// and it will run this code everytime we save it.

console.log('Installing, using nodemon and learn arrow function');

var square = (x) => {
    return x * x;
};

console.log(square(8));