// create javascript object
var obj = {
    name: "Arham",
    age: 25
};
console.log(obj);

// convert obj to string
var stringObj = JSON.stringify(obj);

// check its datatype after strigify
console.log(typeof stringObj);
console.log(stringObj);

// convert string to obj
var newObj = JSON.parse(stringObj);
console.log(typeof newObj);