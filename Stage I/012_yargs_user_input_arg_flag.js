console.log('User Input with Arg and Flag');

// how to let user run our app with argument and flag like
// node abc.js addFile --text="Hari ni dalam sejarah"
// we will be using yargs module
// so install it by using npm install yargs command
// >> npm install yargs

const yargs = require('yargs');
const fs = require('fs');

// lets check what is the different between yargs arg vs standard argv

console.log(process.argv);
console.log(yargs.argv);

// yargs allow us to use these command and parse it correctly
// >> node abc.js addFile --name="contoh.js"
// >> node abc.js addFile --name='contoh.js'
// >> node abc.js addFile --name=contoh.js
// >> node abc.js addFile --name contoh.js


// standard argument will never able to parse these variety of command properly.

// create new file, provide file name, title, author
// node 012_yargs_user_input_arg_flag.js addFile --name="contoh.txt" --title="Contoh File Guna Yargs" --author="Arham"

const argv = yargs.argv;
// to capture arg flag, let say --name="xxx", 
console.log(argv.name);

// function to create a file and popuate it with some text
var command = process.argv[2];
var name = argv.name;
var title = argv.title;
var author = argv.author;
var fileName = './012/' + name;

if (command === 'addFile') {
    console.log('Adding new file', fileName, title, author);

    if (fs.existsSync(fileName)) {
        fs.unlinkSync(fileName);
    }

    fs.appendFileSync(fileName, "Title : " + title + "\n" + "Author: " + author);
}