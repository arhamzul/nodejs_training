console.log('Listening at 127.0.0.1:8080');

const http = require('http');
const fs = require('fs');

http.createServer(function (req, res) {
    // Assync read. It will read the file, and the result will be populated in data.
    fs.readFile('A3.html', function (err, data) {
        // if got error
        if (err) throw err;

        // if got no error, write response, with html head
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        res.write(data);
        res.end();
    });
}).listen(8080);