console.log('Listening...');
console.log('Open up: http://127.0.0.1:8080/?name=Arham&role=admin')

const http = require('http');
const url = require('url');

http.createServer(function (req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });

    // req.url get current URL content
    // url.parse, process URL content and separate its parameter
    var query = url.parse(req.url, true).query;
    // var myText = query.name + " " + query.role;
    var myText = `Your name is ${query.name} and your role is ${query.role.toUpperCase()}`;
    res.write(myText);
    res.end();
}).listen(8080);