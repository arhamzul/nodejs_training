console.log('Starting Require Own File');

// how to create an external file or module
// how to export function and variable so others can use 
// how to import our own external module
// how to call variable and function from external module

const note = require('./007/note.js');

console.log(note.mesej);
console.log(note.owner());

// exercise:
// create an external module name kira.js. 
// in this kira.js, create a function isipaduKotak, which accept 3 parameter
// panjang, lebar dan tinggi
// function ini akan return isipadu kotak (p x l x t), dan terhad kepada 2 titik perpuluhan sahaja
// then file ini akan panggil module kira.js ini like so..

const kira = require('./007/kira.js');

console.log("Result: " + kira.isipaduKotak(10, 10, 10) + " meter padu");