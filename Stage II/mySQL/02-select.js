// select statement

const mysql = require('mysql');

var connnection = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "",
    database: "favolara"
});

connnection.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    var query = "SELECT * FROM users";

    connnection.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(result);
        connnection.end();
    });
});