// select statement

const mysql = require('mysql');

var connection = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "",
    database: "favolara"
});

connection.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    var query = "SELECT * FROM users";

    connection.query(query, function (err, result, fields) {
        if (err) throw err;
        console.log(fields);
        console.log('---- field selection ----');
        console.log(fields[2].name);
    });

    connection.end();
});