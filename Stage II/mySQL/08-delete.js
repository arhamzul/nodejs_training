// insert statement

const mysql = require('mysql');

var connection = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "",
    database: "favolara"
});

connection.connect(function (err) {
    if (err) throw err;
    console.log("Database Connected!");

    var query = "DELETE FROM memos WHERE title = 'Memo Hari Ini'";

    connection.query(query, function (err, result) {
        if (err) throw err;
        console.log("1 record deleted");
        console.log(result);
    });

    connection.end();
});