// inserting many documents (user records) into collections (table)

const mongodb = require('mongodb');
const bcrypt = require('bcrypt');

var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        // successfull connection
        // use db name mofdb
        var dbo = db.db("mofdb");

        var usersObj = [{
                name: "Arham Zulqarnaen",
                role: "Developer",
                email: 'arhamzul@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'active'
            },
            {
                name: "Faeza Seri",
                role: "Developer",
                email: 'arhamzul@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'active'
            },
            {
                name: "Amier Zulfaeiz",
                role: "Developer",
                email: 'faeiz@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'active'
            },
            {
                name: "Zarina Ishak",
                role: "Developer",
                email: 'zarina@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'inactive'
            },
            {
                name: "Marina Khan",
                role: "Admin",
                email: 'marina@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'active'
            },
            {
                name: "Abdullah Ismail",
                role: "User",
                email: 'abdullah@gmail.com',
                password: bcrypt.hashSync('CyperJaya22', 10),
                status: 'active'
            }
        ];
        dbo.collection("users").insertMany(usersObj, function (err, res) {
            if (err) throw err;
            console.log("Number of documents inserted: " + res.insertedCount);
            db.close();
        });
    }
});