// WHERE AND

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");

        // *** instruction : Add another 2 developers. One of them has 'inactive' status ***

        // equivalent ->where('role', 'Admin')->where('status', 'active') in laravel
        var query = {
            role: "Developer",
            status: "active"
        };
        dbo.collection("users").find(query).toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
        });

    }
});