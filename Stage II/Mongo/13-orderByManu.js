// orderBy, Multi

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // orderBy('name', 'asc')->orderBy('status', 'asc')
        var mysort = {
            name: 1,
            status: 1
        };
        dbo.collection("users").find().sort(mysort).toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            db.close();
        });

    }
});