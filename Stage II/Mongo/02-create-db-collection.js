// we will create a db name mofdb AND a collection name users
// db name : mofdb
// then we will create collections (table)

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        // successfull connection
        // use db name mofdb
        var dbo = db.db("mofdb");

        // create collection (table) named 
        dbo.createCollection("users", function (err, res) {
            if (err) throw err;
            console.log("Collection users created!");
            db.close();
        });
    }
});