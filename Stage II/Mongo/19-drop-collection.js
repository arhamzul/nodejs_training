// DROP TABLE (Collection)

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // drop table
        dbo.dropCollection("users", function (err, dropSuccess) {
            if (err) throw err;
            if (dropSuccess) {
                console.log("Collection deleted");
            }
            db.close();
        });

    }
});