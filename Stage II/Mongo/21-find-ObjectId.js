// Find by objectId

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectId = mongodb.ObjectID;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // where condition
        var query = {
            // kalau kita guna nodejs mongodb UUID ObjectId generator waktu insert, then 
            // untuk search kita kena pakai objectId as wll
            _id: new ObjectId('5b5ab0fdb8b72692e57a52d7')
        };
        dbo.collection("articles").find(query).toArray(function (err, result) {
            if (err) throw err;
            console.log(result[0]);
            db.close();
        });

    }
});