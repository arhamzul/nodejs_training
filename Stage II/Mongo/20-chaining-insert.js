// inserting 1 user document (record) into users collection (table)

const mongodb = require('mongodb');
const bcrypt = require('bcrypt');

var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        // successfull connection
        // use db name mofdb
        var dbo = db.db("mofdb");

        var myUser = {
            // _id: 1,
            name: "Arham Zulqarnaen",
            role: "Developer",
            email: 'arhamzul@gmail.com',
            password: bcrypt.hashSync('CyperJaya22', 10),
            status: 'active'
        };
        dbo.collection("users").insertOne(myUser, function (err, res) {
            if (err) throw err;
            console.log("1 document of user inserted");
            var userId = res.insertedId;

            // once user added, we want to add its article as well
            var myArticle = {
                title: 'Macbook Pro i9 Thermal Issue',
                body: 'Early reviewers report the latest Macbook Pro i9 suffer from severe thermal issue when .....',
                status: 'active',
                user_id: userId
            }

            dbo.collection("articles").insertOne(myArticle, function (err, res) {
                if (err) throw err;
                console.log("1 document of article inserted");
            });

            db.close();
        });



    }
});