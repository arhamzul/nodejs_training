// Update Record

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // Where condition untuk kita nak update
        var myquery = {
            email: "arhamzul@gmail.com"
        };

        // nilai baru 
        var newvalues = {
            $set: {
                email: "zulqarnaen@gmail.com",
                name: "Arham Zulqarnaen bin Shamsudin"
            }
        };
        dbo.collection("users").updateOne(myquery, newvalues, function (err, res) {
            if (err) throw err;
            console.log("1 document updated");
            db.close();
        });

    }
});