// simple sample of connection and db creation


var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

// deprecated technique
MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    console.log("Database created!");
    db.close();
});

// current technique
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log(err);
    } else {
        console.log('connected to ' + url);
        db.close();
    }
});