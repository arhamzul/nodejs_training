// SELECT A, B, C

const mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = "mongodb://127.0.0.1:27017";

// connect to mongodb
MongoClient.connect(url, {
    useNewUrlParser: true
}, function (err, db) {
    if (err) {
        console.log('error');
    } else {
        var dbo = db.db("mofdb");
        // equivalent ->select(name, role) in laravel
        dbo.collection("users").find({}, {
            password: 0
        }).toArray(function (err, result) {
            if (err) throw err;
            console.log(result);
            console.log('Third result: ');
            console.log(result[2]);
            db.close();
        });

    }
});