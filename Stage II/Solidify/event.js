var events = require('events');
var eventEmitter = new events.EventEmitter();


// If event scream emitted, do this
eventEmitter.on('scream', () => {
    console.log('I hear a scream!');
});

// If event touch emitted, do this
eventEmitter.on('touch', () => {
    console.log('Dont touch me!');
});

// Fire the 'scream' event:
eventEmitter.emit('scream');

// Fire touch event:
eventEmitter.emit('touch');