// install express
// npm install express

// use nodemon

const express = require('express');

var app = express();

app.get('/', (req, res) => {
  // send HTML
  // res.send('<h1>Hello Express!</h1>');

  // send JSON
  res.send({
    name: 'Andrew',
    likes: [
      'Biking',
      'Cities'
    ]
  });
});

app.get('/about', (req, res) => {
  res.send('About Page');
});

// /bad - send back json with errorMessage
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle request'
  });
});

app.listen(3000);