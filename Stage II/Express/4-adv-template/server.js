const express = require('express');
const hbs = require('hbs');

var app = express();

// register partial di sini
hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

// using helper, so calling year ni x perlu untuk setiap page.
// mcm @blade view composer dlm laravel
// dlm fail hbs, just panggil {{ getCurrentYear }} 
hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});

// helper that able to recieve parameter
hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
});

app.get('/', (req, res) => {
  res.render('home.hbs', {
    pageTitle: 'Home Page',
    welcomeMessage: 'Welcome to my website'
  });
});

app.get('/about', (req, res) => {
  res.render('about.hbs', {
    pageTitle: 'About Page'
  });
});

// /bad - send back json with errorMessage
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle request'
  });
});

app.listen(3000, () => {
  console.log('Server is up on port 3000');
});