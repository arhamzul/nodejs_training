// install express
// install handlebar (handlebarsjs.com) :: View engine like blade
// npm install hbs

const express = require('express');
const hbs = require('hbs');

var app = express();

// we set view engine for express = hbs
// files in folder views
app.set('view engine', 'hbs');

// autoload file in public as static files
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.render('home.hbs', {
    pageTitle: 'Home Page',
    welcomeMessage: 'Welcome to my website',
    currentYear: new Date().getFullYear()
  });
});

app.get('/about', (req, res) => {
  res.render('about.hbs', { // declare the view page
    pageTitle: 'About Page', // set variable
    currentYear: new Date().getFullYear() // another variable
  });
});

// /bad - send back json with errorMessage
app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unable to handle request'
  });
});

app.listen(3000, () => {
  console.log('Server is up on port 3000');
});